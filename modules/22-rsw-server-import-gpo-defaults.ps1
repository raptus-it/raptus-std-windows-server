# Usage:
#
# .\22-rsw-server-import-gpo-defaults.ps1 [-Export]
#
# Only set Export Parameter if creating backup of GPO and/or updating GIT ressources

#PARAM
Param (
    [Parameter(Mandatory=$false)]
    [switch]$Export
)

#FUNCTIONS
function Write-Log {

    [CmdletBinding()] 
    Param (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()] 
        [string]$Message,

        [Parameter(Mandatory=$false)]
        [ValidateSet("Information","Warning","Error")]
        [string]$LogLevel="Information",

        [Parameter(Mandatory=$false)]
        [int]$EventID=27002,

        [Parameter(Mandatory=$false)]
        [string]$Source = (Get-Item $MyInvocation.ScriptName).Name,

        [Parameter(Mandatory=$false)]
        [switch]$Console

    )
    Begin {
        $VerbosePreference = 'Continue'
        $startmsg = Get-EventLog -list | Where-Object {$_.logdisplayname -eq "Raptus"}
        if (-Not $startmsg) {
            New-EventLog -LogName Raptus -Source "Raptus" -ErrorAction SilentlyContinue
            Write-EventLog -LogName Raptus -Source "Raptus" -EntryType Information -EventId 27001 -Message "Raptus Log Start"
        }
        $checksource = [System.Diagnostics.EventLog]::SourceExists($Source);
        if (-Not $checksource) {
            New-EventLog -LogName Raptus -Source $Source
        }
    }
    Process {
        if ($Console -and $LogLevel -eq "Information") {
            Write-Host -ForegroundColor green $Message
        } elseif ($Console -and $LogLevel -eq "Warning") {
            Write-Host -ForegroundColor yellow $Message
        } elseif ($Console -and $LogLevel -eq "Error") {
            Write-Host -ForegroundColor red $Message
        }
    Write-EventLog -LogName Raptus -Message $Message -EntryType $LogLevel -EventId $EventID -Source $Source
    }
    End { }
}

Function Export-GPOs {
    $GPO=Get-GPO -All
    foreach ($Entry in $GPO) {
        $Path=$ExportFolder+$entry.Displayname
        New-Item -ItemType directory -Path $Path
        Backup-GPO -Guid $Entry.id -Path $Path
    }
}

Function Import-GPOs {
    $Folder=Get-childItem -Path $Importfolder -Exclude *.ps1
    foreach ($Entry in $Folder) {
        $Name=$PreName+$Entry.Name+$postname
        $Path=$Importfolder+$entry.Name
        $ID=Get-ChildItem -LiteralPath $Path | Where-Object { $_.Extension -ne ".xml"}        
        New-GPO -Name $Name
        Import-GPO -TargetName $Name -Path $Path -BackupId $ID.Name.Trim('{}')
    }
}

# GPO Linking by Prefix
#
# 10* = Domain Root
# 11* = Domain Controller only
# 12* = All Servers (Domain Member and Domain Controller)
# 13* = [TBD]
# 14* = Clients and Domain Member Server only
# 15* = Clients only
#
Function Connect-GPOs {
    $Folder=Get-childItem -Path $Importfolder -Exclude *.ps1
    foreach ($Entry in $Folder) {
        $Name=$PreName+$Entry.Name+$postname
        Try {
            If ($Name -like "10*") {
                New-GPLink -Name $Name -Target "$dn" -LinkEnabled Yes -ErrorAction Stop
            } elseif ($Name -like "11*") {
                New-GPLink -Name $Name -Target "OU=Domain Controllers,$dn" -LinkEnabled Yes -ErrorAction Stop
            } elseif ($Name -like "12*") {
                New-GPLink -Name $Name -Target "OU=Domain Controllers,$dn" -LinkEnabled Yes -ErrorAction Stop
                New-GPLink -Name $Name -Target "OU=RDSH,OU=$PrimaryOU,$dn" -LinkEnabled Yes -ErrorAction Stop
                New-GPLink -Name $Name -Target "OU=SERVER,OU=$PrimaryOU,$dn" -LinkEnabled Yes -ErrorAction Stop
            } elseif ($Name -like "13*") {
                
            } elseif ($Name -like "14*") {
                New-GPLink -Name $Name -Target "OU=RDSH,OU=$PrimaryOU,$dn" -LinkEnabled Yes -ErrorAction Stop
                New-GPLink -Name $Name -Target "OU=SERVER,OU=$PrimaryOU,$dn" -LinkEnabled Yes -ErrorAction Stop
                New-GPLink -Name $Name -Target "OU=CLIENT,OU=$PrimaryOU,$dn" -LinkEnabled Yes -ErrorAction Stop
            } elseif ($Name -like "15*") {
                New-GPLink -Name $Name -Target "OU=CLIENT,OU=$PrimaryOU,$dn" -LinkEnabled Yes -ErrorAction Stop
            }
        } Catch {
            Write-Log -Message $_.Exception.Message -LogLevel Error -Console
        }
    }
}

function DownloadGitHubRepository 
{ 
    param( 
       [Parameter(Mandatory=$True)] 
       [string] $Name, 
         
       [Parameter(Mandatory=$False)] 
       [string] $Author = "raptus-it", 
         
       [Parameter(Mandatory=$False)] 
       [string] $Branch = "master", 
         
       [Parameter(Mandatory=$False)] 
       [string] $Location = "c:\raptus\rsw"
    ) 
     
    # Force to create a zip file 
    $ZipFile = "$Location\$Name.zip"
    New-Item $ZipFile -ItemType File -Force
 
    $RepositoryZipUrl = "https://bitbucket.org/$Author/$Name/get/$Branch.zip" 
    # download the zip 
    Write-Log -Message "Starting downloading the GitHub Repository" -Console
    Invoke-RestMethod -Uri $RepositoryZipUrl -OutFile $ZipFile
    Write-Log -Message "Download finished" -Console
 
    #Extract Zip File
    Write-Log -Message "Starting unzipping the GitHub Repository locally" -Console
    Expand-Archive -Path $ZipFile -DestinationPath $location -Force
    Write-Log -Message "Unzip finished, Repo Downloaded to: $Location" -Console

    # remove the zip file
    Get-ChildItem -Directory -Path $Location -Filter "*$Name*" | Rename-Item -NewName $Name
    Remove-Item -Path $ZipFile -Force
}

#MODULES
Import-Module GroupPolicy

#VARS
$forestname = (Get-ADDomain).Forest
$dn = (Get-ADDomain).DistinguishedName
$PrimaryOU = $env:userdomain
$ExportFolder="C:\_GPO\"
$ImportFolder="C:\raptus\rsw\raptus-std-windows-server\ressources\grouppolicies\"
$PreName=""
$PostName=""

#Download GIT Repo if not there
If (-Not (Test-Path $ImportFolder)) {
    Try {
        DownloadGitHubRepository -Name "raptus-std-windows-server"
    } Catch {
        Write-Log -Message $_.Exception.Message -LogLevel Error -Console
    }
}

#Move Items to central storage
$sourceitempath = "C:\raptus\rsw\raptus-std-windows-server\ressources\items"
$targetitempath = "\\$forestname\SYSVOL\$forestname\items"

If (-Not (Test-Path $targetitempath)) {
    Try {
        New-Item -Path $targetitempath -type directory -Force -ErrorAction Stop
    } Catch {
        Write-Log -Message $_.Exception.Message -LogLevel Error -Console
    }
}
Write-Log -Message "Copying items to sysvol" -Console
robocopy $sourceitempath $targetitempath /E

#Move GPOs to central storage
$sourcepolpath = "C:\Windows\PolicyDefinitions"
$targetpolpath = "\\$forestname\SYSVOL\$forestname\policies\PolicyDefinitions"

If (-Not (Test-Path $targetpolpath)) {
    Try {
        New-Item -Path $targetpolpath -type directory -Force -ErrorAction Stop
    } Catch {
        Write-Log -Message $_.Exception.Message -LogLevel Error -Console
    }
}
Write-Log -Message "Copying policy definition to sysvol" -Console
robocopy $sourcepolpath $targetpolpath /E

# Run Export only if Parameter set
if ($Export) {
    Try {
        Export-GPOs
        Write-Log -Message "GPOs Exported" -Console
    } Catch {
        Write-Log -Message $_.Exception.Message -LogLevel Error -Console
    }
# Else Run Import
} else {
    Try {
        Import-GPOs
        Write-Log -Message "GPOs Imported" -Console
    } Catch {
        Write-Log -Message $_.Exception.Message -LogLevel Error -Console
    }
    # Run Linking
    Try {
        Connect-GPOs
        Write-Log -Message "GPOs Linked" -Console
    } Catch {
        Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

}


#eof