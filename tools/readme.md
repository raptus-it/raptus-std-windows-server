# Windows Server Standard Tools

### Create an Azure AD Service Principal (create-azure-ad-service-principal.ps1)

This script will do the following:

* Create a local selfsigned certificate and store it in the Windows Certificate-Vault and at the location c:\raptus\selfsigned\
* Create an Azure App registration and links a Service Principal to it
* Gives the Service Principal "Global Reader" rights on the Azure Ressources and "Reader" on the Subscription

The Script takes two params:

* [-servicename]: choose a name for your app connection, that corresponds to the purpose (i.e. "prtgmon")
* [-domain]: customer azuread domain

You might have to give the Principal more rights to make it suitable for your intended purpose