#FUNCTIONS
function Write-Log {

    [CmdletBinding()] 
    Param (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()] 
        [string]$Message,

        [Parameter(Mandatory=$false)]
        [ValidateSet("Information","Warning","Error")]
        [string]$LogLevel="Information",

        [Parameter(Mandatory=$false)]
        [int]$EventID=27002,

        [Parameter(Mandatory=$false)]
        [string]$Source = (Get-Item $MyInvocation.ScriptName).Name,

        [Parameter(Mandatory=$false)]
        [switch]$Console
    )
    Begin {
        if ($Console -and $LogLevel -eq "Information") {
            Write-Host -ForegroundColor green $Message
        } elseif ($Console -and $LogLevel -eq "Warning") {
            Write-Host -ForegroundColor yellow $Message
        } elseif ($Console -and $LogLevel -eq "Error") {
            Write-Host -ForegroundColor red $Message
        }         
        if ((Get-Host).Version.Major -gt 5) {
            exit 0    
        }
    } Process {
        $VerbosePreference = 'Continue'
        $startmsg = Get-EventLog -list | Where-Object {$_.logdisplayname -eq "Raptus"}
        if (-Not $startmsg) {
            New-EventLog -LogName Raptus -Source "Raptus" -ErrorAction SilentlyContinue
            Write-EventLog -LogName Raptus -Source "Raptus" -EntryType Information -EventId 27001 -Message "Raptus Log Start"
        }
        $checksource = [System.Diagnostics.EventLog]::SourceExists($Source);
        if (-Not $checksource) {
            New-EventLog -LogName Raptus -Source $Source
        }
    } End {
        Write-EventLog -LogName Raptus -Message $Message -EntryType $LogLevel -EventId $EventID -Source $Source
    }
}

function DownloadGitHubRepository 
{ 
    param( 
       [Parameter(Mandatory=$True)] 
       [string] $Name, 
         
       [Parameter(Mandatory=$False)] 
       [string] $Author = "raptus-it", 
         
       [Parameter(Mandatory=$False)] 
       [string] $Branch = "master", 
         
       [Parameter(Mandatory=$False)] 
       [string] $Location = "c:\raptus\rsw"
    ) 
     
    # Force to create a zip file 
    $ZipFile = "$Location\$Name.zip"
    New-Item $ZipFile -ItemType File -Force
 
    #$RepositoryZipUrl = "https://bitbucket.org/raptus-it/raptus-std-windows-server/get/master.zip"
    $RepositoryZipUrl = "https://bitbucket.org/$Author/$Name/get/$Branch.zip" 
    # download the zip 
    Write-Log -Message "Starting downloading the GitHub Repository" -Console
    Invoke-RestMethod -Uri $RepositoryZipUrl -OutFile $ZipFile
    Write-Log -Message "Download finished" -Console
 
    #Extract Zip File
    Write-Log -Message "Starting unzipping the GitHub Repository locally" -Console
    Expand-Archive -Path $ZipFile -DestinationPath $location -Force
    Write-Log -Message "Unzip finished, Repo Downloaded to: $Location" -Console

    # remove the zip file
    Get-ChildItem -Directory -Path $Location -Filter "*$Name*" | Rename-Item -NewName $Name
    Remove-Item -Path $ZipFile -Force
}

# Download Bitbucket Repo
Try {
    DownloadGitHubRepository -Name "raptus-std-windows-server"
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

#eof