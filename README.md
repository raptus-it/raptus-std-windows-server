
# Raptus Windows Server Standards #

  

### Description
* This script enforces some basic windows server standards
### Todo
* n/a
### Known Issues
* In an Azure setup, the "raptus.admin" will be set as "Enterprise Administrator". However, this group is excluded, from connecting through RDP, after applying the standard policies. To fix this, remove the "raptus.admin" from the group "Enterprise Administrator".
### Get started
```shell
Set-ExecutionPolicy -ExecutionPolicy Unrestricted

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

(new-object net.webclient).DownloadFile("https://bitbucket.org/raptus-it/raptus-std-windows-server/raw/master/00-rsw-server-kickstart-deployment.ps1","00-rsw-server-kickstart-deployment.ps1"); .\00-rsw-server-kickstart-deployment.ps1
```

### Parts  
##### Windows Server tiny Bootstrapper
* It's recommended to set a proper computername
```powershell
Rename-Computer -NewName "HOSTNAME"
```
* Bootstraps Windows Server with some basic settings. It will create the local administrator "raptus.admin" and set a random password, if no password given
* Script will report the generated password to the EventLog and NinjaRMM Custom Field, if NinjaRMMAgent is installed.

```powershell
C:\Raptus\rsw\raptus-std-windows-server\modules\10-rsw-server-bootstrap.ps1 -RaptusAdminPass "password"
```
##### Windows Server provision Domain Controller
* NetBiosName is optional. If not set, NetBiosName will be DomainName without TLD
* **Careful! If the DomainName has any special character your MUST set the NetBiosName**

```powershell
C:\Raptus\rsw\raptus-std-windows-server\modules\20-rsw-server-create-domain-controller.ps1 -DomainName "cust.tld" [-NetBiosName "cust"] -DSRMpassword "password"
```
##### Windows Server create AD OU skeletton
* PrimaryOU is optional. If not set, primary OU will be named after NetBiosName

```powershell
C:\Raptus\rsw\raptus-std-windows-server\modules\21-rsw-server-set-ad-defaults.ps1 [-PrimaryOU "CUST"]
```
##### Windows Server import basic GPOs
* Only set Export Parameter if creating backup of GPO and/or updating GIT ressources

```powershell
C:\Raptus\rsw\raptus-std-windows-server\modules\22-rsw-server-import-gpo-defaults.ps1 [-Export]
```

Azure Windows Systems have no Built-In Administrators. So the GPO "1002 [USR] Built-In Administration Warning" can be removed.