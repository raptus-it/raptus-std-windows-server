# Usage:
#
# .\21-rsw-server-set-ad-defaults.ps1 [-PrimaryOU "CUST"]
#
# PrimaryOU is optional. If not set, primary OU will be named after NetBiosName

#PARAM
Param (
    [Parameter(Mandatory=$false)]
    [string]$PrimaryOU
)

if (-Not $PrimaryOU) {
    $PrimaryOU = $env:userdomain
}

#FUNCTIONS
function Write-Log {

    [CmdletBinding()] 
    Param (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()] 
        [string]$Message,

        [Parameter(Mandatory=$false)]
        [ValidateSet("Information","Warning","Error")]
        [string]$LogLevel="Information",

        [Parameter(Mandatory=$false)]
        [int]$EventID=27002,

        [Parameter(Mandatory=$false)]
        [string]$Source = (Get-Item $MyInvocation.ScriptName).Name,

        [Parameter(Mandatory=$false)]
        [switch]$Console

    )
    Begin {
        $VerbosePreference = 'Continue'
        $startmsg = Get-EventLog -list | Where-Object {$_.logdisplayname -eq "Raptus"}
        if (-Not $startmsg) {
            New-EventLog -LogName Raptus -Source "Raptus" -ErrorAction SilentlyContinue
            Write-EventLog -LogName Raptus -Source "Raptus" -EntryType Information -EventId 27001 -Message "Raptus Log Start"
        }
        $checksource = [System.Diagnostics.EventLog]::SourceExists($Source);
        if (-Not $checksource) {
            New-EventLog -LogName Raptus -Source $Source
        }
    }
    Process {
        if ($Console -and $LogLevel -eq "Information") {
            Write-Host -ForegroundColor green $Message
        } elseif ($Console -and $LogLevel -eq "Warning") {
            Write-Host -ForegroundColor yellow $Message
        } elseif ($Console -and $LogLevel -eq "Error") {
            Write-Host -ForegroundColor red $Message
        }
    Write-EventLog -LogName Raptus -Message $Message -EntryType $LogLevel -EventId $EventID -Source $Source
    }
    End { }
}

## vars
$dn = (Get-ADDomain).DistinguishedName
$ErrorActionPreference = [System.Management.Automation.ActionPreference]::Stop

## Generate AD structure
Try {
    New-ADOrganizationalUnit -Name $PrimaryOU.ToUpper() -Path $dn
    Write-Log -Message "Primary OU $PrimaryOU created" -Console

    New-ADOrganizationalUnit -Name "CLIENT" -Path "OU=$PrimaryOU,$dn" -Description "Client Computers"
    New-ADOrganizationalUnit -Name "CORPORATION" -Path "OU=$PrimaryOU,$dn" -Description "Security Objects"
    New-ADOrganizationalUnit -Name "RDSH" -Path "OU=$PrimaryOU,$dn" -Description "Remotedesktop Sessionhosts"
    New-ADOrganizationalUnit -Name "SERVER" -Path "OU=$PrimaryOU,$dn" -Description "Server Computer"
    Write-Log -Message "Secondary OUs created" -Console

    New-ADOrganizationalUnit -Name "Divisions" -Path "OU=CORPORATION,OU=$PrimaryOU,$dn" -Description "User Division"
    New-ADOrganizationalUnit -Name "Functions" -Path "OU=CORPORATION,OU=$PrimaryOU,$dn" -Description "User Function"
    New-ADOrganizationalUnit -Name "Security Groups" -Path "OU=CORPORATION,OU=$PrimaryOU,$dn" -Description "Share- and File-access"
    New-ADOrganizationalUnit -Name "System Objects" -Path "OU=CORPORATION,OU=$PrimaryOU,$dn" -Description "System Group Objects"
    New-ADOrganizationalUnit -Name "User Objects" -Path "OU=CORPORATION,OU=$PrimaryOU,$dn" -Description "Real User and Group Objects"
    New-ADOrganizationalUnit -Name "Applications" -Path "OU=SERVER,OU=$PrimaryOU,$dn" -Description "Functions"
    New-ADOrganizationalUnit -Name "Services" -Path "OU=SERVER,OU=$PrimaryOU,$dn" -Description "Functions"
    New-ADOrganizationalUnit -Name "Hypervisor" -Path "OU=SERVER,OU=$PrimaryOU,$dn" -Description "Functions"
    Write-Log -Message "Tertiary OUs created" -Console

    New-ADOrganizationalUnit -Name "Mainfolders" -Path "OU=Security Groups,OU=CORPORATION,OU=$PrimaryOU,$dn" -Description "Root-Folders contained in network shares"
    New-ADOrganizationalUnit -Name "Shares" -Path "OU=Security Groups,OU=CORPORATION,OU=$PrimaryOU,$dn" -Description "Share access"
    Write-Log -Message "Security Group OUs created" -Console

    New-ADOrganizationalUnit -Name "Administrative Accounts" -Path "OU=User Objects,OU=CORPORATION,OU=$PrimaryOU,$dn" -Description "Useraccounts with administrative rights"
    New-ADOrganizationalUnit -Name "Personal Accounts" -Path "OU=User Objects,OU=CORPORATION,OU=$PrimaryOU,$dn" -Description "Real personalized Useraccounts"
    New-ADOrganizationalUnit -Name "Service Accounts" -Path "OU=User Objects,OU=CORPORATION,OU=$PrimaryOU,$dn" -Description "Useraccounts for running services"
    New-ADOrganizationalUnit -Name "Shared Accounts" -Path "OU=User Objects,OU=CORPORATION,OU=$PrimaryOU,$dn" -Description "Unpersonalized shared accounts"
    Write-Log -Message "User Object OUs created"  -Console

} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

## Redirect computer objects
Try {
    redircmp "OU=CLIENT,OU=$PrimaryOU,$dn"
    Write-Log -Message "Client Computer redirected to: OU=CLIENT,$dn" -Console
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

## Raptus Domain Administrator
$username = "raptus.admin"
$userprincipal = $username + "@" + (Get-ADDomain).Forest
$oupath = "OU=Administrative Accounts,OU=User Objects,OU=CORPORATION,OU=$PrimaryOU,$dn"

Try {
    Write-Log -Message "Elevating Raptus Admin to Domain Admin" -Console

    Get-ADUser $username | Move-ADObject -TargetPath $oupath
    Get-ADUser $username | Rename-ADObject -NewName "Raptus Domain Administrator"
    Set-ADUser -Identity $username `
        -DisplayName "Raptus Domain Administrator" `
        -Description "Raptus Domain Administrator" `
        -GivenName "Raptus" `
        -Surname "Domain Administrator" `
        -SamAccountName $username `
        -UserPrincipalName $userprincipal `
    
    Add-ADGroupMember -Identity "Domain Admins" -Members $username
    
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

Try {
    Write-Log -Message "Removing unnecessary privileged users" -Console

    $entadmins = (Get-ADGroupMember -Identity "Enterprise Admins").name
    foreach ($etpadmin in $entadmins) {
        Remove-ADGroupMember -Identity "Enterprise Admins" -Members $etpadmin
    }
    
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

#eof