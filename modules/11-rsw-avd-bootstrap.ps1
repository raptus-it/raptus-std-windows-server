# Usage:
#
# .\11-rsw-avd-bootstrap.ps1
#

#PARAMS
Param (
    [parameter(Mandatory=$true)]
    [string]$RaptusAdminPass
)

#FUNCTIONS
function Write-Log {

    [CmdletBinding()] 
    Param (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()] 
        [string]$Message,

        [Parameter(Mandatory=$false)]
        [ValidateSet("Information","Warning","Error")]
        [string]$LogLevel="Information",

        [Parameter(Mandatory=$false)]
        [int]$EventID=27002,

        [Parameter(Mandatory=$false)]
        [string]$Source = (Get-Item $MyInvocation.ScriptName).Name,

        [Parameter(Mandatory=$false)]
        [switch]$Console

    )
    Begin {
        $VerbosePreference = 'Continue'
        $startmsg = Get-EventLog -list | Where-Object {$_.logdisplayname -eq "Raptus"}
        if (-Not $startmsg) {
            New-EventLog -LogName Raptus -Source "Raptus" -ErrorAction SilentlyContinue
            Write-EventLog -LogName Raptus -Source "Raptus" -EntryType Information -EventId 27001 -Message "Raptus Log Start"
        }
        $checksource = [System.Diagnostics.EventLog]::SourceExists($Source);
        if (-Not $checksource) {
            New-EventLog -LogName Raptus -Source $Source
        }
    }
    Process {
        if ($Console -and $LogLevel -eq "Information") {
            Write-Host -ForegroundColor green $Message
        } elseif ($Console -and $LogLevel -eq "Warning") {
            Write-Host -ForegroundColor yellow $Message
        } elseif ($Console -and $LogLevel -eq "Error") {
            Write-Host -ForegroundColor red $Message
        }
    Write-EventLog -LogName Raptus -Message $Message -EntryType $LogLevel -EventId $EventID -Source $Source
    }
    End { }
}

#Set correct Timezone
Try {
    Set-TimeZone -Id "W. Europe Standard Time"
    Write-Log -Message "Timezone set" -Console
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

#Install basic features
Try {
        Add-WindowsCapability -Online -Name "SNMP.Client~~~~0.0.1.0" -ErrorAction Stop
        Add-WindowsCapability -Online -Name "WMI-SNMP-Provider.Client~~~~0.0.1.0" -ErrorAction Stop
        Write-Log -Message "Feature $winfeature installed" -Console
} Catch {
        Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

#Basic feature config
Try {
    New-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\services\SNMP\Parameters\ValidCommunities" -Name "public" -Value 4 -ErrorAction Stop
    Remove-ItemProperty -Path "HKLM:\SYSTEM\CurrentControlSet\services\SNMP\Parameters\PermittedManagers" -Name "1" -ErrorAction Stop
    Restart-Service -Name "SNMP"
    Write-Log -Message "SNMP configured" -Console
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

#Set Powerstates
$cpws = Get-CimInstance -classname Win32_PowerPlan -Namespace "root\cimv2\power" | Where-Object {$_.IsActive -eq $True}
If ($cpws.ElementName -notmatch "High performance") {
    Try {
        $pws = Get-CimInstance -classname Win32_PowerPlan -Namespace "root\cimv2\power" | Where-Object {$_.ElementName -eq "High performance"}
        Invoke-CimMethod -InputObject $pws -MethodName Activate -ErrorAction Stop
        Write-Log -Message "Powerstate set to High performance" -Console
    } Catch {
        Write-Log -Message $_.Exception.Message -LogLevel Error -Console
    }
}

#Register Powershell Repository
Register-PSRepository -Default -Force

#Set NTPS
Try {
    Start-Service w32time
    $ntps = w32tm /query /computer:$server /configuration | ?{$_ -match 'ntpserver:'} | %{($_ -split ":\s\b")[1]}

    If ($ntps -NotLike "*ch.pool.ntp.org*") {
        Stop-Service w32time
        w32tm /config /manualpeerlist:ch.pool.ntp.org /syncfromflags:ALL
        Start-Service w32time
    }
    w32tm /resync
    Write-Log -Message "NTP set" -Console
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

#eof