# Usage:
#
# .\20-rsw-server-create-domain-controller.ps1 -DomainName "cust.tld" [-NetBiosName "cust"] -DSRMpassword "password"
#
# NetBiosName is optional. If not set, domain name will be the NBName, without TLD

#PARAM
Param (
    [Parameter(Mandatory=$true)]
    [string]$DomainName,
    [Parameter(Mandatory=$false)]
    [string]$NetBiosName,
    [Parameter(Mandatory=$true)]
    [string]$DSRMpassword
)

#FUNCTIONS
function Write-Log {

    [CmdletBinding()] 
    Param (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()] 
        [string]$Message,

        [Parameter(Mandatory=$false)]
        [ValidateSet("Information","Warning","Error")]
        [string]$LogLevel="Information",

        [Parameter(Mandatory=$false)]
        [int]$EventID=27002,

        [Parameter(Mandatory=$false)]
        [string]$Source = (Get-Item $MyInvocation.ScriptName).Name,

        [Parameter(Mandatory=$false)]
        [switch]$Console

    )
    Begin {
        $VerbosePreference = 'Continue'
        $startmsg = Get-EventLog -list | Where-Object {$_.logdisplayname -eq "Raptus"}
        if (-Not $startmsg) {
            New-EventLog -LogName Raptus -Source "Raptus" -ErrorAction SilentlyContinue
            Write-EventLog -LogName Raptus -Source "Raptus" -EntryType Information -EventId 27001 -Message "Raptus Log Start"
        }
        $checksource = [System.Diagnostics.EventLog]::SourceExists($Source);
        if (-Not $checksource) {
            New-EventLog -LogName Raptus -Source $Source
        }
    }
    Process {
        if ($Console -and $LogLevel -eq "Information") {
            Write-Host -ForegroundColor green $Message
        } elseif ($Console -and $LogLevel -eq "Warning") {
            Write-Host -ForegroundColor yellow $Message
        } elseif ($Console -and $LogLevel -eq "Error") {
            Write-Host -ForegroundColor red $Message
        }
    Write-EventLog -LogName Raptus -Message $Message -EntryType $LogLevel -EventId $EventID -Source $Source
    }
    End { }
}

if (-Not $NetBiosName) {
    $NetBiosName = $DomainName.split(".")[0]
}

$dsrmpw = ConvertTo-SecureString -String $DSRMpassword -AsPlainText -Force

Try {
    $interface = Get-NetIPConfiguration -InterfaceAlias "Ethernet" -ErrorAction Stop
    Set-DnsClientServerAddress -InterfaceIndex $interface.InterfaceIndex -ServerAddresses ($interface.IPv4Address.IPAddress,$interface.IPv4DefaultGateway.NextHop) -ErrorAction Stop
    Write-Log -Message "Interface identified and configured - ip: $($interface.IPv4Address.IPAddress), gw: $($interface.IPv4DefaultGateway.NextHop)" -Console
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

Try {
    Install-WindowsFeature -Name AD-Domain-Services -IncludeManagementTools -ErrorAction Stop
    Write-Log -Message "Installing AD Domain Services" -Console

    Install-ADDSForest `
        -DomainName $DomainName `
        -CreateDnsDelegation:$false `
        -DatabasePath "C:\Windows\NTDS" `
        -DomainMode "WinThreshold" `
        -DomainNetbiosName $NetBiosName `
        -ForestMode "WinThreshold" `
        -InstallDNS:$true `
        -LogPath "C:\Windows\NTDS" `
        -NoRebootOnCompletion:$True `
        -SysvolPath "C:\Windows\SYSVOL" `
        -SafeModeAdministratorPassword $dsrmpw `
        -Force:$true
        -ErrorAction Stop

} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

# Set NTPS as a Domain Controller
Try {
    Stop-Service w32time
    w32tm /config /manualpeerlist:ch.pool.ntp.org /syncfromflags:ALL /reliable:YES
    Write-Log -Message "NTP set as RELIABLE" -Console
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

#Restart
Restart-Computer -ComputerName localhost -Force

#eof