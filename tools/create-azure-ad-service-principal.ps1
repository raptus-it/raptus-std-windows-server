# creates a selfsigned certificate and a service principal in azure
# this is usualy used for PRTG custom sensors or some kind of automation
# 
# PARAMS:
# -servicename: choose a name for your app connection, that corresponds to the purpose (i.e. "prtgmon")
# -domain: customer azuread domain
#
# -servicename "<logical name>" -domain "<customer.tld>"
#
# Register-PSRepository -Default
# Install-Module Az
# Install-Module AzureAD

#PARAMS
Param (
    [parameter(Mandatory=$true)]
    [string]$servicename,
    
    [Parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()] 
    [string]$domain
)

#FUNCTIONS
function Write-Log {

    [CmdletBinding()] 
    Param (
        [Parameter(Mandatory=$true)]
        [ValidateNotNullOrEmpty()] 
        [string]$Message,

        [Parameter(Mandatory=$false)]
        [ValidateSet("Information","Warning","Error")]
        [string]$LogLevel="Information",

        [Parameter(Mandatory=$false)]
        [int]$EventID=27002,

        [Parameter(Mandatory=$false)]
        [string]$Source = (Get-Item $MyInvocation.ScriptName).Name,

        [Parameter(Mandatory=$false)]
        [switch]$Console

    )
    Begin {
        $VerbosePreference = 'Continue'
        $startmsg = Get-EventLog -list | Where-Object {$_.logdisplayname -eq "Raptus"}
        if (-Not $startmsg) {
            New-EventLog -LogName Raptus -Source "Raptus" -ErrorAction SilentlyContinue
            Write-EventLog -LogName Raptus -Source "Raptus" -EntryType Information -EventId 27001 -Message "Raptus Log Start"
        }
        $checksource = [System.Diagnostics.EventLog]::SourceExists($Source);
        if (-Not $checksource) {
            New-EventLog -LogName Raptus -Source $Source
        }
    }
    Process {
        if ($Console -and $LogLevel -eq "Information") {
            Write-Host -ForegroundColor green $Message
        } elseif ($Console -and $LogLevel -eq "Warning") {
            Write-Host -ForegroundColor yellow $Message
        } elseif ($Console -and $LogLevel -eq "Error") {
            Write-Host -ForegroundColor red $Message
        }
    Write-EventLog -LogName Raptus -Message $Message -EntryType $LogLevel -EventId $EventID -Source $Source
    }
    End { }
}

function Generate-RandomPassword {
    function Get-RandomCharacters($length, $characters) {
        $random = 1..$length | ForEach-Object { Get-Random -Maximum $characters.length }
        $private:ofs=""
        return [String]$characters[$random]
    }
        
    function Scramble-String([string]$inputString){     
        $characterArray = $inputString.ToCharArray()   
        $scrambledStringArray = $characterArray | Get-Random -Count $characterArray.Length     
        $outputString = -join $scrambledStringArray
        return $outputString 
    }
    $password = Get-RandomCharacters -length 8 -characters 'abcdefghiklmnoprstuvwxyz'
    $password += Get-RandomCharacters -length 8 -characters 'ABCDEFGHKLMNPRSTUVWXYZ'
    $password += Get-RandomCharacters -length 2 -characters '123456789'
    $password += Get-RandomCharacters -length 2 -characters '£_-:;$][!?)(/&%*@+'

    return Scramble-String $password
}

#Generate SelfSigned Certificate
$currentDate = Get-Date
$endDate = $currentDate.AddYears(5)
$notAfter = $endDate.AddYears(5)
$pwd = Generate-RandomPassword
$path = "c:\raptus\selfsigned"
$hostname = Invoke-Command -ScriptBlock {hostname}
$dnsname = "$hostname.$servicename-$domain"

If  (!(Test-Path $path)) {
    New-Item -ItemType Directory -Force -Path $path
}

Try {
    $thumb = (New-SelfSignedCertificate -CertStoreLocation cert:\localmachine\my -DnsName $dnsname -KeyExportPolicy Exportable -Provider "Microsoft Enhanced RSA and AES Cryptographic Provider" -NotAfter $notAfter -ErrorAction Stop).Thumbprint
    $pwd | Out-File -FilePath $path\$dnsname.txt
    $pwd = ConvertTo-SecureString -String $pwd -Force -AsPlainText
    Export-PfxCertificate -cert "cert:\localmachine\my\$thumb" -FilePath $path\$dnsname.pfx -Password $pwd -ErrorAction Stop 
    Write-Log -Message "SelfSigned Certificate Generated" -Console
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

# Login to Azure AD PowerShell With Admin Account
Connect-AzureAD 

# Load the certificate
Try {
    $cert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate("$path\$dnsname.pfx", $pwd) -ErrorAction Stop
    $keyValue = [System.Convert]::ToBase64String($cert.GetRawCertData())
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}

# Create the Azure Active Directory Application
Try {
    $application = New-AzureADApplication -DisplayName $servicename -IdentifierUris "https://$servicename.$domain"
    New-AzureADApplicationKeyCredential -ObjectId $application.ObjectId -CustomKeyIdentifier $servicename -StartDate $currentDate -EndDate $endDate -Type AsymmetricX509Cert -Usage Verify -Value $keyValue
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}


# Create the Service Principal and connect it to the Application
Try {
    $sp=New-AzureADServicePrincipal -AppId $application.AppId
    # Give the Service Principal Reader access to the current tenant (Get-AzureADDirectoryRole)
    $RoleTemplate = Get-AzureADDirectoryRoleTemplate | Where-Object {$_.DisplayName -eq "Global Reader"}
    Enable-AzureADDirectoryRole -RoleTemplateId $RoleTemplate.ObjectId
    Add-AzureADDirectoryRoleMember -ObjectId (Get-AzureADDirectoryRole | where-object {$_.DisplayName -eq "Global Reader"}).Objectid -RefObjectId $sp.ObjectId
    # Make the Service Principal Subscription Reader
    $Subscription = Get-AzureRmContext
    $SubscriptionId = $Subscription.Subscription.SubscriptionId
    New-AzureRmRoleAssignment -ApplicationId $sp.ApplicationId  -Scope "/subscriptions/$SubscriptionId" -RoleDefinitionName "Reader"
} Catch {
    Write-Log -Message $_.Exception.Message -LogLevel Error -Console
}